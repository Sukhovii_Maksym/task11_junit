package view;

import model.LongestPlateau;
import model.Minesweeper;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;

public class MyView {

    private Map<String, String> menu;
    private Map<String, Printable> methodsMenu;
    private static Scanner input = new Scanner(System.in);

    private LongestPlateau lp = new LongestPlateau();
    private Minesweeper mw = new Minesweeper();

    public MyView() {

        menu = new LinkedHashMap<>();
        menu.put("1", "  1 - generate new array with your parameters");
        menu.put("2", "  2 - generate new array with default parameters(1,9,20)");
        menu.put("3", "  3 - find plateau");
        menu.put("4", "  4 - generate new mines field with your parameters");
        menu.put("5", "  5 - generate new mines field with default parameters(20,20,40)");
        menu.put("6", "  6 - show all bombs ");
        menu.put("7", "  7 - show all field with counts of bombs");
        menu.put("Q", "  Q - exit");

        methodsMenu = new LinkedHashMap<>();
        methodsMenu.put("1", this::pressButton1);
        methodsMenu.put("2", this::pressButton2);
        methodsMenu.put("3", this::pressButton3);
        methodsMenu.put("4", this::pressButton4);
        methodsMenu.put("5", this::pressButton5);
        methodsMenu.put("6", this::pressButton6);
        methodsMenu.put("7", this::pressButton7);
    }

    private void pressButton1() {
        int min, max, size;
        System.out.println("Set minimal value of element");
        min = input.nextInt();
        System.out.println("Set maximal value of element");
        max = input.nextInt();
        System.out.println("Set length of your array");
        size = input.nextInt();
        lp.generateNewArray(min, max, size);
    }

    private void pressButton2() {
        lp.generateNewArray();
    }

    private void pressButton3() {
        lp.print();
        lp.findPlateau();
    }

    private void pressButton4() {
        int width, height, probability;
        System.out.println("Set field width");
        width = input.nextInt();
        System.out.println("Set field height");
        height = input.nextInt();
        System.out.println("Set probability of bombs");
        probability = input.nextInt();
        mw = new Minesweeper(width, height, probability);
    }

    private void pressButton5() {
        mw = new Minesweeper(20, 20, 40);

    }

    private void pressButton6() {
        mw.printBombs();
    }

    private void pressButton7() {
        mw.printSolution();
    }

    //-------------------------------------------------------------------------

    private void outputMenu() {
        System.out.println("\nMENU:");
        for (String str : menu.values()) {
            System.out.println(str);
        }
    }

    public void show() {
        String keyMenu;
        do {
            outputMenu();
            System.out.println("Please, select menu point.");
            keyMenu = input.nextLine().toUpperCase();
            try {
                methodsMenu.get(keyMenu).print();
            } catch (Exception e) {
            }
        } while (!keyMenu.equals("Q"));
    }
}
