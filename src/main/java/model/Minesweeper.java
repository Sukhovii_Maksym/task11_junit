package model;

public class Minesweeper {
    private boolean[][] bombs;
    private int[][] solution;
    private int width;
    private int height;
    private int bombProbability;

    public Minesweeper(int width, int height, int bombProbability) {
        this.width = width;
        this.height = height;
        this.bombProbability = bombProbability;
        bombs = new boolean[width + 2][height + 2];
        for (int i = 0; i < width + 2; i++) {
            for (int j = 0; j < height + 2; j++) {
                bombs[i][j] = i != 0 && i != width + 1
                        && j != 0 && j != height + 1
                        && Math.random() * 100 <= (double) (bombProbability);
            }
        }
        solution = new int[width][height];
        for (int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                if (!bombs[i + 1][j + 1]) {
                    int count = 0;
                    for (int k = 0; k < 3; k++) {
                        for (int l = 0; l < 3; l++) {
                            if (bombs[i + k][j + l]) {
                                count++;
                            }
                        }
                    }
                    solution[i][j] = count;
                } else solution[i][j] = 10;
            }
        }
    }

    public Minesweeper() {

    }

    public void printBombs() {
        for (int i = 0; i < width + 2; i++) {
            System.out.println();
            for (int j = 0; j < height + 2; j++) {
                if (bombs[i][j]) System.out.print("# ");
                else System.out.print("0 ");
            }

        }
    }

    public void printSolution() {
        for (int i = 0; i < width; i++) {
            System.out.println();
            for (int j = 0; j < height; j++) {
                if (solution[i][j] == 10) {
                    System.out.print("# ");
                } else {
                    System.out.print(solution[i][j] + " ");
                }
            }

        }
    }

    public int getBombCount() {
        int count = 0;
        for (int i = 0; i < width + 2; i++) {
            for (int j = 0; j < height + 2; j++) {
                if (bombs[i][j]) count++;
            }
        }
        return count;
    }

    public int getCellCount() {
        return height * width;
    }

    public boolean[][] getBombs() {
        return bombs;
    }

    public void setBombs(boolean[][] bombs) {
        this.bombs = bombs;
    }

    public int[][] getSolution() {
        return solution;
    }

    public void setSolution(int[][] solution) {
        this.solution = solution;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBombProbability() {
        return bombProbability;
    }

    public void setBombProbability(int bombProbability) {
        this.bombProbability = bombProbability;
    }

}
