package model;

public class LongestPlateau {
    int[] arr;

    public LongestPlateau(int[] arr) {
        this.arr = arr;
    }

    public LongestPlateau() {

    }

    public int[] getArr() {
        return arr;
    }

    public void setArr(int[] arr) {
        this.arr = arr;
    }

    public int[] generateNewArray(int min, int max, int length) {
        int[] arr = new int[length];
        for (int i = 0; i < length; i++) {
            arr[i] = (int) (Math.random() * (max - min) + min);
        }
        setArr(arr);
        return arr;
    }

    public int[] generateNewArray() {
        return generateNewArray(1, 9, 20);
    }

    public void print() {
        System.out.println("This array: ");
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
    }

    public void findPlateau() {
        int startIndex = 0;
        int length = 0;
        int value = 0;
        for (int i = 1; i < arr.length - 1; i++) {
            int thisPlateau = 0;
            for (int j = i; j < arr.length - 1; j++) {
                if (arr[j] == arr[i]) {
                    thisPlateau++;
                } else break;
            }
            if (thisPlateau > length
                    && arr[i - 1] < arr[i]
                    && arr[i + thisPlateau] < arr[i + thisPlateau - 1]) {
                value = arr[i];
                length = thisPlateau;
                startIndex = i;
            }
        }
        if (length <= 1) {
            System.out.println("\nIn this array absent such plateau");
        } else
            System.out.println("\nThe longest plateau in this array has " + length + " and starts in " +
                    "" + startIndex + " position" + " this value is " + value);
    }
}
